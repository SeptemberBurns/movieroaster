package ua.october.movieroaster;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ua.october.movieroaster.API.MovieClient;

public class MoviePresenter {

    MovieListView movieListView;

    public MoviePresenter(MovieListView movieListView) {
        this.movieListView = movieListView;
    }

    public void presentMovies(){
        MovieClient movieClient = new MovieClient();
        movieClient.getMovies().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( responce -> movieListView.displayMovieList(responce.movies));
    }

}
