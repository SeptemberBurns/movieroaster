package ua.october.movieroaster;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ua.october.movieroaster.Adapters.RecyclerAdapter;

public class MovieListFragment extends Fragment implements MovieListView{

    @BindView(R.id.main_list) RecyclerView recyclerView;
    RecyclerAdapter adapter = new RecyclerAdapter();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.movie_list_fragment,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        MoviePresenter presenter = new MoviePresenter(this);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
        recyclerView.setAdapter(adapter);
        presenter.presentMovies();
    }

    @Override
    public void displayMovieList(List<Movie> movies) {
        adapter.setData(movies);
    }
}
