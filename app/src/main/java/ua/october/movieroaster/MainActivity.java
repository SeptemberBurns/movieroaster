package ua.october.movieroaster;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.List;
import java.util.concurrent.CountDownLatch;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import ua.october.movieroaster.API.MoviesAPI;
import ua.october.movieroaster.API.Responce;
import ua.october.movieroaster.Adapters.RecyclerAdapter;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setTitle("New movies");
        showList();

    }
    public void showList(){
        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.animator.become_visible,R.animator.go_transperent).replace(R.id.main_fragment_container, new MovieListFragment()).commit();
    }

    public void showLoading(){
        getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment_container,new LoadFragment()).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.action_search:
                showLoading();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
