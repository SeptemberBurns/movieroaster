package ua.october.movieroaster.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ua.october.movieroaster.Movie;
import ua.october.movieroaster.R;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    List<Movie> data;

    public RecyclerAdapter(List<Movie> data){ //TODO consider removing
        this.data = data;
    }

    public RecyclerAdapter(){
        this.data = new ArrayList<>();
    }

    public void setData(List<Movie> movies){
        this.data = movies;
        notifyDataSetChanged();
    }

    public void addData(List<Movie> movies) {
        this.data.addAll(movies);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.movie_list_item,viewGroup,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Glide.with(viewHolder.posterImage.getContext())
                .applyDefaultRequestOptions(new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL))
                .load(data.get(i).getPosterPath()).into(viewHolder.posterImage);
        viewHolder.movieTitle.setText(data.get(i).getTitle());
    }

    @Override
    public int getItemCount() {
        if(data!=null) {
            return data.size();
        } else {
            return 0;
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.movie_poster) ImageView posterImage;
        @BindView(R.id.movie_title) TextView movieTitle;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
