package ua.october.movieroaster;

import java.util.List;

public interface MovieListView {

    void displayMovieList(List<Movie> movies);

}
