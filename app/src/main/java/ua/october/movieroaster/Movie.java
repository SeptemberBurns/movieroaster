package ua.october.movieroaster;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Movie implements Serializable {

    String title;

    private static String IMAGE_URL = "http://image.tmdb.org/t/p/original";

    @SerializedName("poster_path")
    String posterPath;

    public String getPosterPath(){
        return IMAGE_URL + posterPath;
    }

    public String getTitle(){
        return title;
    }
}
