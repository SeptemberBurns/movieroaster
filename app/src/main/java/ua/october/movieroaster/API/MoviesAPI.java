package ua.october.movieroaster.API;

import io.reactivex.Single;
import retrofit2.http.GET;
import ua.october.movieroaster.API.Responce;

public interface MoviesAPI {

    @GET("discover/movie?api_key=0b8d0d81eb552b104c8fd2863cb5d5b7")
    Single<Responce> getMovies();

}
