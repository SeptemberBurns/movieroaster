package ua.october.movieroaster.API;

import io.reactivex.Single;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class MovieClient {

    public static final String BASE_URL = "http://api.themoviedb.org/3/";

    MoviesAPI moviesAPI;
    Retrofit retrofit;

    public MovieClient() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        moviesAPI = retrofit.create(MoviesAPI.class);
    }

    public Single<Responce> getMovies() {
        return moviesAPI.getMovies();
    }

}
